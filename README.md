# Test RedDotPayment
## Requirements
- PHP 7.1.3
- NodeJS & Npm
- Composer
- Webpack
- Docker

## Init

```bash
    $ rm -Rf .data
    $ composer install
    $ cp .env.dist .env
    $ docker-compose build
    $ docker-compose up -d
    $ bin/console doctrine:migrations:execute 20180212072448
    $ bin/console doctrine:migrations:execute 20180212112938
    $ bin/console doctrine:migrations:execute 20180219203710
    $ npm install
    $ npm run dev
```

Check docker db IP
```bash
    $ docker inspect rdp_db_1 | grep IPAddress
```
with this IP you can set .env file to set db config. Remember if you want to use bin/console from
local, you have to use 127.0.0.1 insted of docker inspect IP.

## url
http://localhost
