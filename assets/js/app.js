// We need to declarate in window global, bec webpack change the name of vars and scope.
window.add2Cart = (ev, id) => {
    ev.preventDefault();

    const qty = parseInt($('#product-addCart-' + id + ' :input[name="qty"]').val());
    $.ajax({
        url: 'http://localhost/api/cart',
        method: 'POST',
        data: {
            id,
            quantity: qty,
        }
    }).done(function( result ) {
        console.log(result);
    });
};
