<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ProductRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function findHomeProducts()
    {
        return $this->createQueryBuilder('p')
            ->where('p.stock > 0')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
}
