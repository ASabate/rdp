<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Product as Product;
use App\Entity\Cart as Cart;
use App\Entity\Purchase as Purchase;
use App\Entity\PurchaseItem as PurchaseItem;

class ShoppingController extends Controller
{
    /**
     * Products list, you can add products to cart
     * Return Twig index view
     */
    public function index()
    {
        $request = Request::createFromGlobals();

        if (empty($request->cookies->get('rdp'))) {
            $hash = bin2hex(random_bytes(16));
            $request->cookies->set('rdp', $hash);
        }

        // @TODO Just to test in localhost
        $hash = '1234';

        $repository = $this->getDoctrine()->getRepository(Product::class);
        $repositoryCart = $this->getDoctrine()->getRepository(Cart::class);
        $products = $repository->findHomeProducts();
        $cart = $repositoryCart->findBy(['cookie' => $hash]);

        $cartByProduct = [];
        foreach ($products as $key => $product) {
            if ($product->getImage() === '') {
                $products[$key]->setImage('/img/no_image.jpg');
            }

            $cartByProduct[$product->getId()] = 0;
            foreach ($cart as $c) {
                if ($c->getProduct() == $product->getId()) {
                    $cartByProduct[$product->getId()] = $c->getQuantity();
                }
            }
        }

        return $this->render('shopping/index.html.twig', [
            'products' => $products,
            'cart' => $cartByProduct,
            'isCart' => false,
        ]);
    }

    /**
     * Cart list, you can remove products from cart
     * Return Twig cart view
     */
    public function cart() {
        $request = Request::createFromGlobals();

        $rdp = $request->cookies->get('rdp');
        // @TODO Just to test in localhost
        $rdp = '1234';

        if (empty($rdp)) {
            return $this->redirect('/');
        }

        $repository = $this->getDoctrine()->getRepository(Cart::class);
        $repositoryProduct = $this->getDoctrine()->getRepository(Product::class);
        $cart = $repository->findBy(['cookie' => $rdp]);
        $products = [];
        $cartByProduct = [];
        foreach ($cart as $c) {
            $product = $repositoryProduct->find($c->getProduct());

            if ($product->getImage() === '') {
                $product->setImage('/img/no_image.jpg');
            }
            $cartByProduct[$product->getId()] = $c->getQuantity();
            $products[] = $product;
        }

        return $this->render('shopping/index.html.twig', [
            'products' => $products,
            'cart' => $cartByProduct,
            'isCart' => true,
        ]);
    }

    /**
     * Products list no editable, you only can pay in this page.
     * Return Twig checkout view
     */
    public function checkout() {
        $request = Request::createFromGlobals();

        $rdp = $request->cookies->get('rdp');
        // @TODO Just to test in localhost
        $rdp = '1234';

        if (empty($rdp)) {
            return $this->redirect('/');
        }

        $repository = $this->getDoctrine()->getRepository(Cart::class);
        $repositoryProduct = $this->getDoctrine()->getRepository(Product::class);
        $cart = $repository->findBy(['cookie' => $rdp]);
        $products = [];
        $total = 0;
        $totals = [];
        $cartByProduct = [];
        foreach ($cart as $c) {
            $product = $repositoryProduct->find($c->getProduct());

            if ($product->getImage() === '') {
                $product->setImage('/img/no_image.jpg');
            }
            $cartByProduct[$product->getId()] = $c->getQuantity();
            $total += round($c->getQuantity() * $product->getPrice(), 2);
            $totals[$product->getId()] = round($c->getQuantity() * $product->getPrice(), 2);
            $products[] = $product;
        }

        return $this->render('shopping/checkout.html.twig', [
            'products' => $products,
            'cart' => $cartByProduct,
            'totals' => $totals,
            'total' => $total,
        ]);
    }

    /**
     * Thank you page, you buy is completed, show if all is ok or not.
     * Return Twig purchased view
     */
    public function payment() {
        $request = Request::createFromGlobals();
        $em = $this->getDoctrine()->getManager();

        $rdp = $request->cookies->get('rdp');
        // @TODO Just to test in localhost
        $rdp = '1234';
        $email = $request->request->get('stripeEmail');
        $token = $request->request->get('stripeToken');

        if (empty($rdp) || empty($email) || empty($token)) {
            return $this->redirect('/');
        }

        $repository = $this->getDoctrine()->getRepository(Cart::class);
        $repositoryProduct = $this->getDoctrine()->getRepository(Product::class);
        $cart = $repository->findBy(['cookie' => $rdp]);

        \Stripe\Stripe::setApiKey('sk_test_0W1ltTU7hGcNXuvDMusLMjr7');
        $customer = \Stripe\Customer::create([
            'email' => $email,
            'source'  => $token,
        ]);

        $total = 0;
        $products = [];
        foreach ($cart as $c) {
            $product = $repositoryProduct->find($c->getProduct());
            $products[$c->getProduct()] = $product;
            $total += round($c->getQuantity() * $product->getPrice(), 2);
        }

        $charge = \Stripe\Charge::create([
            'customer' => $customer->id,
            'amount'   => $total * 100,
            'currency' => 'sgd',
        ]);

        $purchase = new Purchase();
        $purchase->setStatus($charge->status);
        $purchase->setTotal($charge->amount);
        $purchase->setCurrency($charge->currency);
        $purchase->setStripe($charge->id);
        $purchase->setCustomer($charge->customer);
        $purchase->setEmail($charge->source->name);
        $purchase->setCreatedAt(new \DateTime("now"));

        $em->persist($purchase);
        $em->flush();

        if ($charge->paid && $charge->status == 'succeeded') {
            foreach($cart as $c) {
                $purchaseItem = new PurchaseItem();
                $purchaseItem->setPurchase($purchase->getId());
                $purchaseItem->setProduct($c->getProduct());
                $purchaseItem->setQuantity($c->getQuantity());

                $stock = $products[$c->getProduct()]->getStock() - $c->getQuantity();
                $products[$c->getProduct()]->setStock($stock);

                $em->persist($purchaseItem);
                $em->persist($products[$c->getProduct()]);
                $em->remove($c);
            }
            $em->flush();
            $request->cookies->remove('rdp');

            return $this->render('shopping/purchased_ok.html.twig');
        }

        return $this->render('shopping/purchased_fail.html.twig');
    }
}
