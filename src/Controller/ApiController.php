<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\Cart as Cart;

class ApiController extends Controller
{
    public function home() {
        return $this->json([
            'name' => 'RedDotPaymentTest',
            'version' => '1.0',
        ]);
    }

    /**
     * Add product to cart
     * Return JSON
     */
    public function add2Cart() {
        $request = Request::createFromGlobals();

        $productId = intval($request->request->get('id'));
        $qty = intval($request->request->get('quantity'));
        $rdp = $request->cookies->get('rdp');
        // @TODO Just to test in localhost
        $rdp = '1234';
        $date = new \DateTime("now");

        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(Cart::class);
        $cart = $repository->findOneBy([
            'cookie' => $rdp,
            'product' => $productId,
        ]);

        if (!$cart) {
            $cart = new Cart();
            $cart->setCookie($rdp);
            $cart->setCreatedAt($date);
        }

        if ($qty < 0) {
            return $this->json([
                'error' => 'QTY_SHOULD_BE_POSITIVE',
            ]);
        } else if ($qty == 0) {
            $em->remove($cart);
        } else {
            $cart->setProduct($productId);
            $cart->setQuantity($qty);

            $em->persist($cart);
        }

        $em->flush();

        return $this->json([
            'id' => $cart->getId(),
            'product' => $productId,
            'quantity' => $qty,
        ]);
    }
}
