<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CartRepository")
 * @ORM\Table(indexes={@ORM\Index(name="cookie_idx", columns={"cookie"})})
 */
class Cart
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId() {
        return $this->id;
    }

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cookie;

    public function setCookie($cookie) {
        $this->cookie = $cookie;
    }

    public function getCookie() {
        return $this->cookie;
    }

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Product")
     * @ORM\Column(type="integer")
     */
    private $product;

    public function setProduct($product) {
        $this->product = $product;
    }

    public function getProduct() {
        return $this->product;
    }

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    public function setQuantity($quantity) {
        $this->quantity = $quantity;
    }

    public function getQuantity() {
        return $this->quantity;
    }

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    public function setCreatedAt($createdAt) {
        $this->created_at = $createdAt;
    }

    public function getCreatedAt() {
        return $this->created_at;
    }
}
