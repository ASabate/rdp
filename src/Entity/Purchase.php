<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PurchaseRepository")
 */
class Purchase
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId() {
        return $this->id;
    }

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PurchaseItem", mappedBy="purchase")
     */
    private $items;

    public function getItems() {
        return $this->items;
    }

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $status;

    public function setStatus($status) {
        $this->status = $status;
    }

    public function getStatus() {
        return $this->status;
    }

    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $total;

    public function setTotal($total) {
        $this->total = $total;
    }

    public function getTotal() {
        return $this->total;
    }

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $currency;

    public function setCurrency($currency) {
        $this->currency = $currency;
    }

    public function getCurrency() {
        return $this->currency;
    }

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $stripe;

    public function setStripe($stripe) {
        $this->stripe = $stripe;
    }

    public function getStripe() {
        return $this->stripe;
    }

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $customer;

    public function setCustomer($customer) {
        $this->customer = $customer;
    }

    public function getCustomer() {
        return $this->customer;
    }

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getEmail() {
        return $this->email;
    }

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    public function setCreatedAt($createdAt) {
        $this->created_at = $createdAt;
    }

    public function getCreatedAt() {
        return $this->created_at;
    }
}
