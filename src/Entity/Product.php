<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId() {
        return $this->id;
    }

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    public function setName($name) {
        $this->name = $name;
    }

    public function getName() {
        return $this->name;
    }

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    public function setDescription($description) {
        $this->description = $description;
    }

    public function getDescription() {
        return $this->description;
    }

    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $price;

    public function setPrice($price) {
        $this->price = $price;
    }

    public function getPrice() {
        return $this->price;
    }

    /**
     * @ORM\Column(type="integer")
     */
    private $stock;

    public function setStock($stock) {
        $this->stock = $stock;
    }

    public function getStock() {
        return $this->stock;
    }

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    public function setImage($image) {
        $this->image = $image;
    }

    public function getImage() {
        return $this->image;
    }

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    public function setUpdatedAt($updatedAt) {
        $this->updated_at = $updatedAt;
    }

    public function getUpdatedAt() {
        return $this->updated_at;
    }

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    public function setCreatedAt($createdAt) {
        $this->created_at = $createdAt;
    }

    public function getCreatedAt() {
        return $this->created_at;
    }
}
