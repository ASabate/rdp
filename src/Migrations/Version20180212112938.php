<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180212112938 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE purchase_item (id INT AUTO_INCREMENT NOT NULL, purchase INT NOT NULL, product INT NOT NULL, quantity INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE cart CHANGE cookie_id cookie VARCHAR(255) NOT NULL, CHANGE product_id product INT NOT NULL');
        $this->addSql('CREATE INDEX cookie_idx ON cart (cookie)');
        $this->addSql('ALTER TABLE product ADD description LONGTEXT NOT NULL, ADD image VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE purchase ADD status VARCHAR(20) NOT NULL, ADD total NUMERIC(10, 2) NOT NULL, ADD created_at DATETIME NOT NULL');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE purchase_item');
        $this->addSql('DROP INDEX cookie_idx ON cart');
        $this->addSql('ALTER TABLE cart CHANGE cookie cookie_id VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE product product_id INT NOT NULL');
        $this->addSql('ALTER TABLE product DROP description, DROP image');
        $this->addSql('ALTER TABLE purchase DROP status, DROP total, DROP created_at');
    }
}
